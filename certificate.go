package model
import "time"

type Consignor struct {
	Id       string  `json:"id"`
	Name     Text    `json:"name"`
	RoleCode string  `json:"role_code"`
	Address  Address `json:"address"`
}

type Consignee struct {
	Name     Text    `json:"name"`
	RoleCode string  `json:"role_code"`
	Address  Address `json:"address"`
}

type Port struct {
	Code string `json:"code"`
	Name Text   `json:"name"`
}
type Weight struct {
	UnitCode string  `json:"unit_code"`
	Measure  float32 `json:"measure"`
}
type Package struct {
	LevelCode     string `json:"level_code"`
	TypeCode      string `json:"type_code"`
	Quantity      int    `json:"quantity"`
	ShippingMarks Text   `json:"shipping_marks"`
}
type TransportEquipment struct {
	Id            string `json:"id"`
	AffixedSealId string `json:"affixed_seal_id"`
}
type Period struct {
	Start time.Time `json:"start_date_time"`
	End   time.Time `json:"end_date_time"`
}

type FoodProcess struct {
	TypeCode         string  `json:"type_code"`
	Name             Text    `json:"name"`
	CompletionPeriod Period  `json:"completion_period"`
	OperationCountry Country `json:"operation_country"`
	Operator         Party   `json:"operator"`
}

type FoodItem struct {
	CargoIdentifier              string             `json:"cargo_identifier"`
	Sequence                     int                `json:"sequence"`
	Name                         Text               `json:"name"`
	PhysicalPackage              Package            `json:"physical_package"`
	Description                  Text               `json:"description"`
	NetWeight                    Weight             `json:"net_weight"`
	AdditionalInformation        Note               `json:"additional_information"`
	AssociatedTransportEquipment TransportEquipment `json:"associated_transport_equipment"`
	OriginCountry                Country            `json:"origin_country"`
	AppliedProcesses             []FoodProcess      `json:"applied_processes"`
}

type Text struct {
	Language string `json:"language"`
	Value    string `json:"text"`
}

type Container struct {
	Number string `json:"number"`
	Seal   string `json:"seal"`
}

type Note struct {
	Subject     Text   `json:"subject"`
	ContentCode string `json:"content_code"`
	Content     Text   `json:"content"`
}
type Person struct {
	Name          Text `json:"name"`
	Qualification Text `json:"qualification"`
}
type Party struct {
	Id       string  `json:"id"`
	Name     Text    `json:"name"`
	RoleCode string  `json:"role_code"`
	TypeCode string  `json:"type_code"`
	Person   Person  `json:"person"`
	Address  Address `json:"address"`
}
type Country struct {
	Code string `json:"code"`
	Name Text   `json:"name"`
}
type Address struct {
	PostCode           Text    `json:"post_code"`
	LineOne            Text    `json:"line_one"`
	LineTwo            Text    `json:"line_two"`
	LineThree          Text    `json:"line_three"`
	LineFour           Text    `json:"line_four"`
	LineFive           Text    `json:"line_five"`
	City               Text    `json:"city"`
	Country            Country `json:"country"`
	CountrySubDivision Text    `json:"country_sub_division"`
}
type Location struct {
	Id      string  `json:"id"`
	Name    Text    `json:"name"`
	Address Address `json:"address"`
}
type Signatory struct {
	TypeCode       string    `json:"type_code"`
	ActualDate     time.Time `json:"actual_date"`
	Location       Location  `json:"location"`
	Provider       Party     `json:"provider"`
	IncludedClause string    `json:"included_clause"`
}
type ExaminationEvent struct {
	Id   string `json:"id"`
	Name Text   `json:"name"`
}

type CarriageTransport struct {
	Id                 string `json:"id"`
	ModeCode           string `json:"mode_code"`
	UsedTransportMeans []Text `json:"used_transport"`
}

type Certificate struct {
	Id                              string             `json:"id"`
	Names                           []Text             `json:"names"`
	TypeCode                        string             `json:"type_code"`
	StatusCode                      string             `json:"status_code"`
	IssueDate                       time.Time          `json:"issue_date"`
	Issuer                          Party              `json:"issuer"`
	UnOfficialCommercialInformation Note               `json:"un_official_commercial_information"`
	OfficialInformation             Note               `json:"official_information"`
	Signatory                       []Signatory        `json:"signatory"`
	ExportingCountry                Country            `json:"exporting_country"`
	ReExportingCountry              Country            `json:"re_exporting_country"`
	Consignor                       Consignor          `json:"consignor"`
	Consignee                       Consignee          `json:"consignee"`
	LoadingPort                     Port               `json:"loading_port"`
	ImportCountry                   Country            `json:"import_country"`
	ConsigneeReceiptLocation        Text               `json:"consignee_receipt_location"`
	UnloadingPort                   Port               `json:"unloading_port"`
	ExaminationEvent                []ExaminationEvent `json:"examination_event"`
	MainCarriageTransportMovement   CarriageTransport  `json:"main_carriage_transport_movement"`
	Items                           []FoodItem         `json:"items"`
}

type OpenCertificate struct {
	Id                            string         `json:"id"`
	IssueDate                     time.Time      `json:"issue_date"`
	TypeCode                      string         `json:"type_code"`
	LoadingPort                   string         `json:"loading_port"`
	MainCarriageTransportMovement string         `json:"main_carriage_transport_movement"`
	UnloadingPort                 string         `json:"unloading_port"`
	Items                         []OpenFoodItem `json:"items"`
}

type OpenFoodItem struct {
	Weight           float32 `json:"weight"`
	Quantity         int     `json:"quantity"`
	NumberOfPackages int     `json:"number_of_packages"`
	PackageSize      string  `json:"package_size"`
}

type Payload struct {
	Issuer	string `json:"issuer"`
	Certificate OpenCertificate `json:"certificate"`
	Recipients []string `json:"recipients"`
}